FROM debian:stable AS base

WORKDIR /data
VOLUME /data

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -qy \
    make \
    texlive-full \
    fonts-liberation \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && fc-cache -f -v

FROM base
USER 1000:1000
ENTRYPOINT ["make"]
