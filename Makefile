# Config file
MKRC ?= latexmkrc
INDENT_SETTINGS ?= indent.yaml

# Source .tex file
TOP_TEX_FILE ?= main

# LaTeX compiler output .pdf file
OUTPUT_NAME ?= $(TOP_TEX_FILE)

# Ghostscript-based pdf postprocessing
include compress.mk

# LaTeX version:
# -pdf		= pdflatex
# -pdfdvi	= pdflatex with dvi
# -pdfps	= pdflatex with ps
# -pdfxe	= xelatex with dvi (faster than -xelatex)
# -xelatex	= xelatex without dvi
# -pdflua	= lualatex with dvi  (faster than -lualatex)
# -lualatex	= lualatex without dvi
BACKEND ?= -pdflua

LATEXFLAGS ?= -halt-on-error -file-line-error -shell-escape
LATEXMKFLAGS ?= -silent
BIBERFLAGS ?= --fixinits
REGEXDIRS ?= . # distclean dirs
TIMERON ?= # show CPU usage
TIKZFILE ?= # .tikz file for tikz rule
PRINTFILE ?= # .pdf file to print

# Makefile options
MAKEFLAGS := -s
.DEFAULT_GOAL := main
.NOTPARALLEL:

export NOTESON
export LATEXFLAGS
export BIBERFLAGS
export REGEXDIRS
export TIMERON
export TIKZFILE
export PRINTFILE

define compile
	latexmk -norc -r $(MKRC) $(LATEXMKFLAGS) $(BACKEND) -jobname=$(basename $(2)) $(basename $(1))
endef

%.fmt: %.tex
	etex -ini -halt-on-error -file-line-error \
	-shell-escape -jobname=$(OUTPUT_NAME) \
	"&latex" mylatexformat.ltx """$^"""

.PHONY: main
##! компиляция документа
main:
	$(call compile,$(TOP_TEX_FILE),$(OUTPUT_NAME))

.PHONY: preformat
##! прекомпиляция документа
preformat: $(TOP_TEX_FILE).fmt main

.PHONY: tikz
##! компиляция tikz графики
tikz: BACKEND=-pdflua # некоторые библиотеки работают только с lualatex
tikz:
	$(call compile,images/cache/tikz,$(basename $(notdir $(TIKZFILE))))

.PHONY: print
##! печать файла на 2 страницы на 1
print:
	$(call compile,images/cache/print,$(basename $(notdir $(PRINTFILE)))_print)

define clean-target
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(basename $(2)) -c $(basename $(1))
endef

define distclean-target
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(basename $(2)) -C $(basename $(1))
endef

.PHONY: clean
##! очистка проекта от временных файлов
clean:
	$(call clean-target,$(TOP_TEX_FILE),$(OUTPUT_NAME))

.PHONY: distclean
##! полная очистка проекта от временных файлов
distclean:
	$(call distclean-target,$(TOP_TEX_FILE),$(OUTPUT_NAME))

.PHONY: zip
##! архивирование проекта
zip:
	zip -ry $(OUTPUT_NAME).zip .

INDENT_FILES ?= $(wildcard *.tex)
.PHONY: indent
##! форматирование файлов *.tex
indent:
	@$(foreach file, $(INDENT_FILES),\
	latexindent -l=$(INDENT_SETTINGS) -s -w $(file);)

.PHONY: indent-wrap
##! форматирование файлов *.tex с разбиением длинных строк
indent-wrap: INDENT_SETTINGS+=-m
indent-wrap: indent

# https://gist.github.com/klmr/575726c7e05d8780505a
.PHONY: help
##! это сообщение
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^##! / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^##! //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n##! /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
