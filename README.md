Шаблон оформления НИР для LaTeX.

Основан на статье [Оформление отчета о НИР в LaTeX](http://dkhramov.dp.ua/Comp.NIRReportDSTU300895) и [шаблоне для оформления КД](https://github.com/AndreyAkinshin/Russian-Phd-LaTeX-Dissertation-Template).

# Основные команды

Копирование шаблона.

```bash
git clone https://seregaxvm@bitbucket.org/seregaxvm/nir.git
```

Сборка документа:

```bash
make
```

Очистка временных файлов:

```bash
make clear
```

Стилизация исходного кода:

```bash
make indent
```
